FROM debian:buster
MAINTAINER <christopher.wipf@ligo.org>

RUN apt-get update -qq
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qq wget
RUN wget -nv http://apt.ligo-wa.caltech.edu/debian/pool/buster/cdssoft-release-buster/cdssoft-release-buster_1.0.5_all.deb
RUN dpkg -i cdssoft-release-buster_1.0.5_all.deb
RUN rm cdssoft-release-buster_1.0.5_all.deb
RUN apt-get update -qq
RUN echo "advligorts-common	advligorts/advligorts_users	string" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qq advligorts-cymac
# userspace gpstime/mbuf and softrt
RUN wget -nv http://labcit.ligo.caltech.edu/~wipf/advligorts-awgtpman_4.1.0-1_amd64.deb
RUN wget -nv http://labcit.ligo.caltech.edu/~wipf/advligorts-daqd_4.1.0-1_amd64.deb
RUN wget -nv http://labcit.ligo.caltech.edu/~wipf/advligorts-edc_4.1.0-1_amd64.deb
RUN wget -nv http://labcit.ligo.caltech.edu/~wipf/advligorts-local-dc_4.1.0-1_amd64.deb
RUN wget -nv http://labcit.ligo.caltech.edu/~wipf/advligorts-rcg_4.1.0-1_amd64.deb
RUN DEBIAN_FRONTEND=noninteractive dpkg -i advligorts-awgtpman_4.1.0-1_amd64.deb advligorts-daqd_4.1.0-1_amd64.deb advligorts-edc_4.1.0-1_amd64.deb advligorts-local-dc_4.1.0-1_amd64.deb advligorts-rcg_4.1.0-1_amd64.deb
RUN rm advligorts-awgtpman_4.1.0-1_amd64.deb advligorts-daqd_4.1.0-1_amd64.deb advligorts-edc_4.1.0-1_amd64.deb advligorts-local-dc_4.1.0-1_amd64.deb advligorts-rcg_4.1.0-1_amd64.deb
