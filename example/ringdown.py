#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import ezca
from cdsutils import nds

# design the plant
f = 1e3
Q = 1e4
L = 9.81/(2*np.pi*f)**2

# connect to DAQ and epics channels
daq = nds.connection()
epics = ezca.Ezca(ifo='X1')
force_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_FORCE')
disp_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_DISP')
noise_force_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_NOISE_FORCE')
noise_disp_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_NOISE_DISP')

# disable feedback
force_fm.turn_off('INPUT', 'OUTPUT')
disp_fm.turn_off('OUTPUT')

# turn off noise sources
noise_force_fm.turn_off('OUTPUT')
noise_disp_fm.turn_off('OUTPUT')

# set plant parameters
epics.write('SYS-EXAMPLE_PLANT_LENGTH', L)
epics.write('SYS-EXAMPLE_PLANT_Q', Q)

# clear history
epics.write('SYS-EXAMPLE_PLANT_RESET', 1)

# kick the oscillator
force_fm.turn_on('OFFSET')
force_fm.ramp_offset(100, 0)
force_fm.turn_on('OUTPUT')
force_fm.turn_off('OUTPUT', 'OFFSET')
force_fm.ramp_offset(0, 0)

# get data
data_blocks = []
num_blocks = int(np.ceil(Q/f))
print(f'taking ringdown data for {num_blocks} sec')
for data_block in daq.iterate(0, num_blocks, 1, ['SYS-EXAMPLE_PLANT_DISP_OUT']):
    data_blocks += [data_block[0].data,]
dt = 1/data_block[0].sample_rate

# save data
data = np.concatenate(data_blocks)
np.savez('ringdown.npz', data=data, f=f, Q=Q, dt=dt)

# plot data
t = np.linspace(0, (len(data)-1)*dt, len(data))
envelope = max(data)*np.exp(-2*np.pi*f*t[::10]/(2*Q))
plt.figure()
plt.plot(t, data, marker='.', linestyle='', label='ringdown data')
plt.plot(t[::10], envelope, color='xkcd:orange', lw=3, label=f'f={f} Hz, Q={Q}')
plt.plot(t[::10], -envelope, color='xkcd:orange', lw=3, label='_')
plt.legend()
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.grid(True)
plt.ticklabel_format(axis='y', scilimits=(-3,3))
plt.savefig('ringdown.png')

