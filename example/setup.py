#!/usr/bin/env python3

import os
import ezca
import foton

epics = ezca.Ezca(ifo='X1')

# disable feedback
damp_fm = epics.LIGOFilter('SYS-EXAMPLE_CTRL_DAMP')
damp_fm.turn_off('OUTPUT')

# settings for noise sources
noise_force_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_NOISE_FORCE')
noise_force_fm.turn_on('INPUT', 'OUTPUT', 'OFFSET')
noise_force_fm.ramp_offset(-0.5, 0)
noise_disp_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_NOISE_DISP')
noise_disp_fm.turn_on('INPUT', 'OUTPUT', 'OFFSET')
noise_disp_fm.ramp_offset(-0.5, 0)
noise_disp_fm.ramp_gain(1e-6, 0)

# oscillator settings
epics.write('SYS-EXAMPLE_PLANT_MASS', 0.25)
epics.write('SYS-EXAMPLE_PLANT_LENGTH', 0.25)
epics.write('SYS-EXAMPLE_PLANT_Q', 10000)

# clear history
epics.write('SYS-EXAMPLE_PLANT_RESET', 1)

# damping settings
filter_filename = os.path.dirname(__file__) + '/../chans/X1SYSEXAMPLE.txt'
ff = foton.FilterFile(filter_filename)
ff['EXAMPLE_CTRL_DAMP'][0].name = '0:1000'
ff['EXAMPLE_CTRL_DAMP'][0].design = 'zpk([0], [1000], 1, "n")'
ff.write()
epics.write('FEC-6_LOAD_NEW_COEFF', 1)
damp_fm.turn_on('INPUT', 'FM1')
damp_fm.ramp_gain(-1, 0)
force_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_FORCE')
force_fm.turn_on('INPUT', 'OUTPUT')
disp_fm = epics.LIGOFilter('SYS-EXAMPLE_PLANT_DISP')
disp_fm.turn_on('INPUT', 'OUTPUT')

# enable feedback
damp_fm.turn_on('OUTPUT')

