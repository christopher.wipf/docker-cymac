# Example model
This model has two subsystems: a plant and a controller.

![block diagram](medm/x1sysexample_EXAMPLE.gif)

The plant simulates a harmonic oscillator.  It includes force and sensing noise
sources.  A [symplectic integrator](https://en.wikipedia.org/wiki/Symplectic_integrator)
is used for its stability when modeling high-Q oscillators.

![block diagram](medm/x1sysexample_EXAMPLE_PLANT.gif)

The controller subsystem contains a standard filter module.

![block diagram](medm/x1sysexample_EXAMPLE_CTRL.gif)

## Getting started
- Set up `docker-cymac` and the cdssoft packages on a debian buster host
- Move `x1sysexample.mdl` into the `userapps` directory, next to `x1iop.mdl`
- Start the cymac with `./start_cymac` (`x1sysexample` will be compiled and launched automatically)
- Run `eval $(./env_cymac)` to set up the host environment

## Next steps
Run `medm -x example/medm/x1sysexample.adl` to explore the model's GUI.  (This interface was [generated](https://git.ligo.org/christopher.wipf/mdl2adl) from the Simulink diagram.)

Run `example/setup.py` to configure the plant and a velocity damping controller.  Watch the oscillator's motion on a scope using `ndscope X1:SYS-EXAMPLE_PLANT_DISP_OUT`.

![ndscope screenshot](img/ndscope.png)

Run `example/ringdown.py` to perform a ringdown measurement on the simulated oscillator.  View the resulting plot in `ringdown.png`.

![ringdown plot](img/ringdown.png)
