# docker-cymac
## Instructions
- Needs a Linux host OS (for example, Debian buster)
  - To run control room tools (DTT, ndscope, etc) in the host, add the [cdssoft repo](http://apt.ligo-wa.caltech.edu/debian/) and `sudo apt install cds-workstation`
  - Install other prerequisites: `sudo apt install docker.io docker-compose`
- Place .mdl files to run in `userapps` folder (one should be x1iop)
  - Include `userspacegps=1 posixmbuf=1 softrt=1` in CDS parameters block
  - Adjust `RCG_LIB_PATH` in `advligorts/env` as needed to include Simulink libraries and other source files
- Run `./start_cymac` to launch the virtual cymac
  - All .mdl files in `userapps` will be compiled and started
  - Use `./start_cymac debug` to watch the compilation

