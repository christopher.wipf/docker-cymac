#!/bin/bash
set -e
cd /
. /etc/advligorts/env
export SITE IFO site ifo
SLEEP=5

# use realtime priority for models and local_dc, if available
CHRT="chrt -r 99"
CHRT2="chrt -f 1"
if ! $CHRT true 2>/dev/null; then
  echo "warning: realtime priority not available"
  CHRT=""
  CHRT2=""
fi

# start medm web server (this should eventually be its own service)
apt-get install busybox
busybox httpd -f -p 8080 &

# start rpcbind, needed for awgtpman
rpcbind

# start nds data retrieval helper
mkdir -p /run/nds
nds --rundir /run/nds &

# this directory is needed for IOCs to load filter coeffs
mkdir -p /opt/rtcds/${site}/${ifo}/chans/tmp

# and this is needed for IPCs
mkdir -p /opt/rtcds/${site}/${ifo}/chans/ipc
touch /opt/rtcds/${site}/${ifo}/chans/ipc/${IFO}.ipc

# begin building a systab file for local_dc
echo -n "$(hostname)" >/tmp/systab

build_mdl () {
    mdl=$1
    # compile RCG model
    rtcds build --user-space --no-kernel-space $mdl
}

install_and_start_mdl () {
    mdl=$1

    # populate systab for local_dc
    echo -n " $mdl" >>/tmp/systab

    # install in target directory
    rtcds install $mdl

    # start EPICS IOC
    cd /opt/rtcds/${site}/${ifo}/target/${mdl}/${mdl}epics
    # why do we need this epics path shenanigan?
    EPICS_DB_INCLUDE_PATH=/usr/lib/epics/dbd ./${mdl}epics ${mdl}epics${IFO}.cmd &
    sleep $SLEEP

    # start front end code
    cd /opt/rtcds/${site}/${ifo}/target/${mdl}/bin
    $CHRT ./$mdl -m $mdl &
    sleep $SLEEP

    # add testpoint.par entry for awgtpman
    cd /opt/rtcds/${site}/${ifo}/target/gds/param
    node=$(cat tpchn_${mdl}.par | grep rmid | head -1 | awk '{print $3}')
    echo [${IFO:0:1}-node${node}] >>testpoint.par
    echo hostname=$(hostname) >>testpoint.par
    echo system=$mdl >>testpoint.par

    # start awgtpman
    cd /
    TARGET=/opt/rtcds/${site}/${ifo} awgtpman -s $mdl &
    sleep $SLEEP

    # add daqdrc master config entries
    echo /opt/rtcds/${site}/${ifo}/chans/daq/${mdl^^}.ini >>/tmp/master
    echo /opt/rtcds/${site}/${ifo}/target/gds/param/tpchn_${mdl}.par >>/tmp/master
}

# IOP model is built and started first
if [ "$(ls /opt/rtcds/userapps/*iop.mdl | wc -l)" = "1" ]; then
    iopmdl=$(basename /opt/rtcds/userapps/*iop.mdl .mdl)
    build_mdl $iopmdl
    install_and_start_mdl $iopmdl
else
    echo "error: there must be exactly one IOP model"
    exit 1
fi

# all other models in userapps are built and installed
# models with IPCs have to be built twice
for f in /opt/rtcds/userapps/*.mdl; do
    mdl=$(basename $f .mdl)
    [ "$mdl" = "$iopmdl" ] && continue
    # skip libraries
    grep -q cdsParameters $f || continue
    # skip models that don't contain IPCs
    grep -q IPCx $f || continue
    # first build fails, but populates IPC file
    build_mdl $mdl || true
done

for f in /opt/rtcds/userapps/*.mdl; do
    mdl=$(basename $f .mdl)
    [ "$mdl" = "$iopmdl" ] && continue
    # skip libraries
    grep -q cdsParameters $f || continue
    build_mdl $mdl
    install_and_start_mdl $mdl
done

# make the medm and chans files accessible to the user on the host
chown $HOST_UID -R /opt/rtcds/${site}/${ifo}/medm
chown $HOST_UID -R /opt/rtcds/${site}/${ifo}/chans

# install testpoint.par link
ln -sf /opt/rtcds/${site}/${ifo}/target/gds/param/testpoint.par /tmp/testpoint.par

# start local_dc to provide data to daqd
$CHRT2 local_dc --systab=/tmp/systab &
sleep $SLEEP

# start daqd
daqd -c /etc/advligorts/daqdrc &

# sleep forever (returning would stop the container)
while :; do sleep 3600; done

